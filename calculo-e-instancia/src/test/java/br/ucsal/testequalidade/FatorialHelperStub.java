package br.ucsal.testequalidade;

/**
 * O uso de métodos de instância/publicos nesta classe é intencional e objetiva
 * ilustrar o uso de mocks para métodos de instância.
 * 
 * @author claudioneiva
 *
 */

public class FatorialHelperStub extends FatorialHelper{

	public Long calcularFatorial(int x) {
		switch (x) {
		case 0:
			return 1L;
		case 1:
			return 1L;
		case 2:
			return 2L;
		default:
			throw new RuntimeException("calcularFatorial(" + x + ") não definido em FatorialHelperStub");
		}
	}

}
