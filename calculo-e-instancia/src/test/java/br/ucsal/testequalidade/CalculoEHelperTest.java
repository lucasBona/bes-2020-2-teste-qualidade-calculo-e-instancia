package br.ucsal.testequalidade;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class CalculoEHelperTest {

	private static FatorialHelper fatorialHelperStub;
	private static CalculoEHelper calculoEHelper;

	@BeforeAll
	public static void setup() {
		fatorialHelperStub = new FatorialHelperStub();
		calculoEHelper = new CalculoEHelper(fatorialHelperStub);
	}

	@Test
	public void calcularE2() {
		Integer n = 2;
		Double eEsperado = 2.5;
		Double eAtual = calculoEHelper.calcularE(n);
		assertEquals(eEsperado, eAtual);
	}

}
